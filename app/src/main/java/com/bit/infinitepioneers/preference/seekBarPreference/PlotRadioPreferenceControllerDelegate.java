package com.bit.infinitepioneers.preference.seekBarPreference;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bit.infinitepioneers.paint.R;

/**
 * Created by yuguorui on 2016/7/30.
 * SeekBarPreference的控制逻辑
 */
public class PlotRadioPreferenceControllerDelegate extends PreferenceControllerDelegate {

    PlotRadioPreferenceControllerDelegate(Context context, Boolean isView) {
        super(context, isView);

    }

    @Override
    protected void onBind(View view) {
        if (isView) {
            titleView = (TextView) view.findViewById(android.R.id.title);
            summaryView = (TextView) view.findViewById(android.R.id.summary);

            titleView.setText(title);
            summaryView.setText(summary);
        }

        view.setClickable(false);
        seekBarView = (SeekBar) view.findViewById(R.id.seekbar);
        measurementView = (TextView) view.findViewById(R.id.measurement_unit);
        valueView = (TextView) view.findViewById(R.id.seekbar_value);
        setMaxValue(maxValue);
        seekBarView.setOnSeekBarChangeListener(this);

        measurementView.setText(measurementUnit);

        setCurrentValue(currentValue);
        valueView.setText(String.valueOf((float) currentValue / 16));

        bottomLineView = (FrameLayout) view.findViewById(R.id.bottom_line);
        valueHolderView = (LinearLayout) view.findViewById(R.id.value_holder);

        setDialogEnabled(dialogEnabled);
        setEnabled(isEnabled());
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        int newValue = progress + minValue;

        if (interval != 1 && newValue % interval != 0) {
            newValue = Math.round(((float) newValue) / interval) * interval;
        }

        if (newValue > maxValue) {
            newValue = maxValue;
        } else if (newValue < minValue) {
            newValue = minValue;
        }

        if (changeValueListener != null) {
            if (!changeValueListener.onChange(newValue)) {
                return;
            }
        }
        currentValue = newValue;
        valueView.setText(String.valueOf((float) newValue / 16));
    }

    @Override
    public void onClick(final View v) {
        new CustomValueDialog(context, dialogStyle, minValue, maxValue, currentValue)
                .setPersistValueListener(new PersistValueListener() {
                    @Override
                    public boolean persistInt(int value) {
                        setCurrentValue(value);
                        seekBarView.setOnSeekBarChangeListener(null);
                        seekBarView.setProgress(currentValue - minValue);
                        seekBarView.setOnSeekBarChangeListener(PlotRadioPreferenceControllerDelegate.this);

                        valueView.setText(String.valueOf((float) currentValue / 16));
                        return true;
                    }
                })
                .show();
    }
}
