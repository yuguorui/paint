package com.bit.infinitepioneers.preference.seekBarPreference;

public interface ChangeValueListener {
    boolean onChange(int value);
}