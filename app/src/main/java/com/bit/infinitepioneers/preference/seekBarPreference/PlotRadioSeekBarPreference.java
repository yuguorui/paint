package com.bit.infinitepioneers.preference.seekBarPreference;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import com.bit.infinitepioneers.paint.R;

/**
 * Created by yuguorui on 2016/7/30.
 * 用于绘图精度选择的首选项界面
 */
public class PlotRadioSeekBarPreference extends SeekBarPreference {

    public PlotRadioSeekBarPreference(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs);
    }

    public PlotRadioSeekBarPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    public PlotRadioSeekBarPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public PlotRadioSeekBarPreference(Context context) {
        super(context);
        init(null);
    }

    protected void init(AttributeSet attrs){
        setLayoutResource(R.layout.seekbar_view_layout);
        controllerDelegate = new PlotRadioPreferenceControllerDelegate(getContext(), false);
        controllerDelegate.setViewStateListener(this);
        controllerDelegate.setPersistValueListener(this);
        controllerDelegate.setChangeValueListener(this);
        controllerDelegate.loadValuesFromXml(attrs);
    }

}
