package com.bit.infinitepioneers.preference;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;

import com.bit.infinitepioneers.paint.R;

/**
 * Created by yuguorui on 2016/7/19.
 * 自定义的Fragment
 */
public class CustomPreFragment extends PreferenceFragment {
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
    }
}
