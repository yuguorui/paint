package com.bit.infinitepioneers.preference.seekBarPreference;

/**
 * Created by yuguorui on 2016/7/30.
 */
public interface PersistValueListener {
    boolean persistInt(int value);
}
