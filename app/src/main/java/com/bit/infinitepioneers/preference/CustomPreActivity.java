package com.bit.infinitepioneers.preference;

/**
 * Created by yuguorui on 2016/7/19.
 * 首选项设置页
 */

import android.content.SharedPreferences;
import android.preference.PreferenceActivity;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;

import com.bit.infinitepioneers.paint.PaintView;
import com.bit.infinitepioneers.paint.R;

import java.util.List;

public class CustomPreActivity extends PreferenceActivity {
    @Override
    public void onBuildHeaders(List<Header> target) {
        loadHeadersFromResource(R.xml.headers_preference, target);
    }

    @Override
    protected boolean isValidFragment(String fragmentName) {
        return CustomPreFragment.class.getName().equals(fragmentName);
    }

}
