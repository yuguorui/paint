package com.bit.infinitepioneers.paint;

/**
 * Created by yuguorui on 2016/7/18.
 * TCP 的客户端
 */

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;


public class TcpClient {

    private int server_port;
    private String server_ip;
    private PaintActivity activity;
    private int plot_radio;

    public TcpClient(PaintActivity activity, int server_port, String server_ip, int plot_radio) {
        this.server_ip = server_ip;
        this.server_port = server_port;
        this.activity = activity;
        this.plot_radio = plot_radio;
    }

    public void send(String text) {

        Thread thread = new Thread(new SocketClass(server_port, server_ip, text, plot_radio), "tcp");
        thread.start();
    }


    class SocketClass implements Runnable {
        int server_port;
        String ip;
        String text;
        int plot_radio;

        public SocketClass(int server_port, String ip, String text, int plot_radio) {
            this.server_port = server_port;
            this.ip = ip;
            this.text = text;
            this.plot_radio = plot_radio;
        }

        @Override
        public void run() {
            try {
                Log.d("State", "another thread.");
                Socket socket = new Socket();
                socket.connect(new InetSocketAddress(server_ip, server_port), 2000);
                PrintWriter out = new PrintWriter(socket.getOutputStream(),
                        true);
                //noinspection unused
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        socket.getInputStream()));

                out.println(plot_radio + " " + text);
                socket.close();
            } catch (SocketTimeoutException e) {
                activity.handler.sendEmptyMessage(PaintMessage.TIME_OUT);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
