package com.bit.infinitepioneers.paint;

import android.util.Log;

import java.util.ArrayList;

/**
 * Created by yuguorui on 2016/7/17.
 * 最小二乘法
 */
public class LinearRegression {

    public static PointPair leastSquares(ArrayList<Point> trace) {

        // first pass: read in data, compute xbar and ybar
        double sumx = 0.0, sumy = 0.0, sumx2 = 0.0;
        for (Point point :
                trace) {
            sumx += point.x;
            sumx2 += point.x * point.x;
            sumy += point.y;
        }

        double xbar = sumx / trace.size();
        double ybar = sumy / trace.size();

        // second pass: compute summary statistics
        double xxbar = 0.1, yybar = 0.0, xybar = 0.0;
        for (Point point :
                trace) {
            xxbar += (point.x - xbar) * (point.x - xbar);
            yybar += (point.y - ybar) * (point.y - ybar);
            xybar += (point.x - xbar) * (point.y - ybar);
        }

        double beta1 = xybar / xxbar;
        double beta0 = ybar - beta1 * xbar;

        Point firstPoint = trace.get(0);
        Point lastPoint = trace.get(trace.size() - 1);

        Log.d("STATE", Double.toString(beta1));

        double y1 = beta0 + beta1 * firstPoint.x;
        double y2 = beta0 + beta1 * lastPoint.x;

        if (Double.isNaN(beta1)) {
            return null;
        }

        return new PointPair(new Point(firstPoint.x, (int) Math.round((y1 + firstPoint.y) / 2)),
                new Point(lastPoint.x, (int) Math.round((y2 + lastPoint.y) / 2)));
    }
}
