package com.bit.infinitepioneers.paint;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.bit.infinitepioneers.preference.CustomPreActivity;
import com.bit.infinitepioneers.preference.CustomPreFragment;
import com.bit.infinitepioneers.svg.PathParser;
import com.bit.infinitepioneers.svg.SVG2Traces;
import com.github.angads25.filepicker.controller.DialogSelectionListener;
import com.github.angads25.filepicker.model.DialogConfigs;
import com.github.angads25.filepicker.model.DialogProperties;
import com.github.angads25.filepicker.view.FilePickerDialog;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Scanner;

public class PaintActivity extends AppCompatActivity
        implements GetStringDialogFragment.NoticeDialogListener {
    public static final String EXTRA_DATA = "traces";
    public static final String EXTRA_INDEX = "index";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.paint_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PaintView paintView = (PaintView) findViewById(R.id.paintView);
                paintView.undo();
            }
        });

        @SuppressWarnings("unchecked")
        ArrayList<ArrayList<Point>> traces = (ArrayList<ArrayList<Point>>)
                getIntent().getSerializableExtra(EXTRA_DATA);

        if (traces != null) {
            PaintView paintView = (PaintView) findViewById(R.id.paintView);
            paintView.setAllTraces(traces);
        }
    }

    @SuppressLint("HandlerLeak")
    final Handler handler = new Handler() {
        @Override
        public void handleMessage(Message message) {
            if (message.what == PaintMessage.TIME_OUT) {
                showTips("连接超时，请检查网络。");
            }
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_paint, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.save_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void sendToCar(MenuItem item) {
        int port = PreferenceManager.getDefaultSharedPreferences(this).getInt(
                getResources().getString(R.string.CarPortKey),
                getResources().getInteger(R.integer.ServerPort));
        String ip = PreferenceManager.getDefaultSharedPreferences(this).getString(
                getResources().getString(R.string.CarIpKey),
                getResources().getString(R.string.ServerIP));

//        int plotRadio = PreferenceManager.getDefaultSharedPreferences(this).getInt(
//                getResources().getString(R.string.PlotRadioKey),
//                getResources().getInteger(R.integer.PlotRadio));
        // API调整，绘图大小的调整不在小车端完成，从而实现更好的精度
        TcpClient tcpClient = new TcpClient(this, port, ip,
                PreferenceManager.getDefaultSharedPreferences(this).getInt(
                        getResources().getString(R.string.PlotRadioKey),
                        getResources().getInteger(R.integer.PlotRadio))
        );
        PaintView paintView = (PaintView) findViewById(R.id.paintView);
        Snackbar.make(paintView, getResources().getString(R.string.TipsWhenPaint), Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
        tcpClient.send(paintView.getPositions());
    }

    public void importFile(MenuItem item) {
        DialogProperties properties = new DialogProperties();
        properties.selection_mode = DialogConfigs.SINGLE_MODE;
        properties.selection_type = DialogConfigs.FILE_SELECT;
        properties.root = new File(DialogConfigs.DEFAULT_DIR);
        properties.extensions = new String[]{"paint", "pa", "svg"};

        FilePickerDialog dialog = new FilePickerDialog(this, properties);
        dialog.setDialogSelectionListener(new DialogSelectionListener() {
            @Override
            public void onSelectedFilePaths(String[] files) {
                if (files.length == 0) {
                    return;
                }
                try {
                    FileInputStream fileInputStream = new FileInputStream(new File(files[0]));

                    // 当文件为paint时
                    if (files[0].endsWith("paint") || files[0].endsWith("pa")) {
                        Scanner scanner = new Scanner(fileInputStream);
                        ArrayList<ArrayList<Point>> allTraces = new ArrayList<>();
                        while (scanner.hasNextInt()) {
                            int x = scanner.nextInt(), y = scanner.nextInt(), pen = scanner.nextInt();
                            if (pen == 0) {
                                ArrayList<Point> trace = new ArrayList<>();
                                allTraces.add(trace);
                                trace.add(new Point(x, y));
                            } else {
                                if (allTraces.isEmpty()) {
                                    allTraces.add(new ArrayList<Point>());
                                }
                                ArrayList<Point> trace = allTraces.get(allTraces.size() - 1);
                                trace.add(new Point(x, y));
                            }
                        }
                        scanner.close();


                        PaintView paintView = (PaintView) findViewById(R.id.paintView);
                        paintView.setAllTraces(allTraces);
                        paintView.invalidate();
                    }

                    // 当文件为svg时
                    else if (files[0].endsWith("svg") || files[0].endsWith("SVG")) {
                        PaintView paintView = (PaintView) findViewById(R.id.paintView);
                        PathParser.CubicSubdivision = PreferenceManager.getDefaultSharedPreferences(PaintActivity.this)
                                .getInt(getResources().getString(R.string.SVGCubicBezierPrecisionKey), 8);

                        paintView.setAllTraces(SVG2Traces.SVG2Trace(files[0]));
                        paintView.invalidate();
                    } else {
                        // here should never be excused.
                        throw new UnsupportedOperationException();
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

            }
        });

        dialog.show();
    }

    public void showTips(String content) {
        Snackbar.make(findViewById(R.id.paintView), content, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }

    public void clearPaint(MenuItem item) {
        PaintView paintView = (PaintView) findViewById(R.id.paintView);
        paintView.clear();
    }

    public void savePaint(MenuItem item) {
        GetStringDialogFragment dialogFragment = new GetStringDialogFragment();
        dialogFragment.show(getFragmentManager(), "GetStringFragment");
    }

    public void setPreference(MenuItem item) {
        Intent intent = new Intent(this, CustomPreActivity.class);
        intent.putExtra(CustomPreActivity.EXTRA_SHOW_FRAGMENT, CustomPreFragment.class.getName());
        intent.putExtra(CustomPreActivity.EXTRA_NO_HEADERS, true);
        startActivity(intent);
    }

    //Add this method to show Dialog when the required permission has been granted to the app.
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case FilePickerDialog.EXTERNAL_READ_PERMISSION_GRANT: {
                //noinspection StatementWithEmptyBody
                if (grantResults.length <= 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    //Permission has not been granted. Notify the user.
                    Toast.makeText(this, "需要权限以访问需导入的文件。", Toast.LENGTH_SHORT).show();
                } else {
//                    if(dialog!=null)
//                    {   //Show dialog if the read permission has been granted.
//                        dialog.show();
//                    }
                }
            }
        }
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialogFragment) {
        Dialog dialog = dialogFragment.getDialog();
        EditText editText = (EditText) dialog.findViewById(R.id.edit_text);
        String fileName = editText.getText().toString();
        PaintView view = (PaintView) findViewById(R.id.paintView);
        Bitmap image = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(image);
        view.draw(canvas);
        SavedObject savedObject = new SavedObject(view.getAllTraces(), new BitmapSerializable(image));
        try {
            File dir = new File(getFilesDir(), getResources().getString(R.string.ProjectsPath));
            if (!dir.exists()) {
                dir.mkdir();
            }
            FileOutputStream fileOutputStream = new FileOutputStream(new File(dir.getAbsolutePath() + File.separator + fileName));
            ObjectOutputStream out = new ObjectOutputStream(fileOutputStream);
            out.writeObject(savedObject);
            out.close();
            fileOutputStream.close();
            this.showTips("保存完成。");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
