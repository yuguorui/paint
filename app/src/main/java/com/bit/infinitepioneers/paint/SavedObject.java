package com.bit.infinitepioneers.paint;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by yuguorui on 2016/7/22.
 * 用于保存需要保存的绘图
 */
public class SavedObject implements Serializable{
    public ArrayList<ArrayList<Point>> traces;
    public BitmapSerializable bitmap;

    public SavedObject(ArrayList<ArrayList<Point>> traces, BitmapSerializable bitmap) {
        this.traces = traces;
        this.bitmap = bitmap;
    }
}
