package com.bit.infinitepioneers.paint;

/**
 * Created by yuguorui on 2016/7/17.
 * 用于绘图
 */

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;
import java.util.Formatter;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import static java.lang.Math.*;
import static com.bit.infinitepioneers.paint.LinearRegression.leastSquares;

public class PaintView extends View {
    private ArrayList<ArrayList<Point>> allTraces = new ArrayList<>();
    private ArrayList<Point> segmentTrace;
    private ArrayList<Point> fittingTrace;
    private ArrayList<Point> printTrace;
    private int pointCount = 0;
    int paintResolution = getResources().getInteger(R.integer.PaintResolution);

    public PaintView(Context context, final AttributeSet attrs) {
        super(context, attrs);
        super.setBackgroundColor(Color.WHITE);

        super.setOnTouchListener(new OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                Point point = new Point((int) event.getX(), (int) event.getY());

                pointCount++;

                if (event.getAction() == MotionEvent.ACTION_DOWN) { // 判断按下
                    segmentTrace = new ArrayList<>(); // 开始新的记录
                    segmentTrace.add(point);

                    fittingTrace = new ArrayList<>();

                    printTrace = new ArrayList<>();
                    printTrace.add(point);

                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    segmentTrace.add(point);
                    printTrace.add(point);
//                    ArrayList<Point> tmp = LinearRegression.leastSquares(segmentTrace);
                    ArrayList<Point> tmp = handleMaxVelocityRadio(leastSquares(segmentTrace));
                    if (tmp != null) {
                        fittingTrace.addAll(tmp);
                        allTraces.add(fittingTrace);
                    }
                    fittingTrace = null;
                    segmentTrace = null;
                    printTrace = null;
                    pointCount = 0;
                    PaintView.this.postInvalidate();
                } else if (event.getAction() == MotionEvent.ACTION_MOVE) {
                    segmentTrace.add(point);
                    printTrace.add(point);
                    paintResolution = PreferenceManager.getDefaultSharedPreferences(getContext()).getInt(
                            getResources().getString(R.string.PaintRevolutionKey),
                            getResources().getInteger(R.integer.PaintResolution));
                    if (pointCount % paintResolution == 0) {
//                        ArrayList<Point> tmp = LinearRegression.leastSquares(segmentTrace);
                        ArrayList<Point> tmp = handleMaxVelocityRadio(leastSquares(segmentTrace));
                        fittingTrace.addAll(tmp);
                        segmentTrace = new ArrayList<>();
                        pointCount = 0;
                    }
                    PaintView.this.postInvalidate(); // 重绘
                }
                return true; // 表示下面的不再执行了
            }
        });
    }

    @Override
    protected void onDraw(Canvas canvas) {

        Paint paintLine = new Paint();
        paintLine.setColor(ContextCompat.getColor(getContext(), R.color.colorLine));
        paintLine.setStrokeWidth(PreferenceManager.getDefaultSharedPreferences(getContext()).getInt(
                getResources().getString(R.string.PaintLineWidthKey),
                getResources().getInteger(R.integer.PaintLineWidth)
        ));

        Paint paintSquare = new Paint();
        paintSquare.setColor(ContextCompat.getColor(getContext(), R.color.colorSquare));
        if (printTrace != null && printTrace.size() > 1) {
            Iterator<Point> iterator = printTrace.iterator();
            Point firstPoint = null; // 开始点
            Point lastPoint = null; // 结束点
            while (iterator.hasNext()) {
                // 找到开始点
                if (firstPoint == null) firstPoint = iterator.next();
                else {
                    if (lastPoint != null) {
                        firstPoint = lastPoint;

                    }
                    lastPoint = iterator.next();
                    canvas.drawLine(firstPoint.x, firstPoint.y, lastPoint.x, lastPoint.y, paintLine);
                    // 画线
                }

            }
        }

        int squareWidth = PreferenceManager.getDefaultSharedPreferences(getContext()).getInt(
                getResources().getString(R.string.SquareWidthKey),
                getResources().getInteger(R.integer.SquareWidth)
        );
        for (List<Point> trace :
                allTraces) {
            Point firstPoint = null; // 开始点
            Point lastPoint = null; // 结束点
            if (trace != null && trace.size() > 1) {
                Iterator<Point> iterator = trace.iterator();
                while (iterator.hasNext()) {
                    // 找到开始点
                    if (firstPoint == null) {
                        firstPoint = iterator.next();

                    } else {
                        if (lastPoint != null) {
                            firstPoint = lastPoint;
                        }
                        lastPoint = iterator.next();
                        canvas.drawLine(firstPoint.x, firstPoint.y, lastPoint.x, lastPoint.y, paintLine);
                        // 画线
                        canvas.drawRect(firstPoint.x - squareWidth, firstPoint.y - squareWidth,
                                firstPoint.x + squareWidth, firstPoint.y + squareWidth, paintSquare);
                        canvas.drawRect(lastPoint.x - squareWidth, lastPoint.y - squareWidth,
                                lastPoint.x + squareWidth, lastPoint.y + squareWidth, paintSquare);

                    }

                }
            }
        }

        super.onDraw(canvas);
    }

    public String getPositions() {
        StringBuilder sb = new StringBuilder();
        Formatter formatter = new Formatter(sb, Locale.ENGLISH);
        float plotRadio = PreferenceManager.getDefaultSharedPreferences(getContext()).getInt(
                getResources().getString(R.string.PlotRadioKey),
                getResources().getInteger(R.integer.PlotRadio)) / 16.0f;
        for (List<Point> trace :
                allTraces) {
            for (int i = 0; i < trace.size(); i++) {
                if (i == 0) {
                    formatter.format("%d %d %d ", (int) (trace.get(i).x * plotRadio), (int) (trace.get(i).y * plotRadio), 0);
                } else {
                    formatter.format("%d %d %d ", (int) (trace.get(i).x * plotRadio), (int) (trace.get(i).y * plotRadio), 1);
                }
            }
        }
        formatter.format("%d %d %d\n", -1, -1, -1);
        return sb.toString();
    }

    public void clear() {
        allTraces = new ArrayList<>();
        segmentTrace = null;
        fittingTrace = null;
        printTrace = null;
        pointCount = 0;
        postInvalidate();
    }

    public void setAllTraces(ArrayList<ArrayList<Point>> allTraces) {
        this.allTraces = allTraces;
    }

    public void undo() {
        if (allTraces != null && allTraces.size() > 0)
            allTraces.remove(allTraces.size() - 1);
        fittingTrace = null;
        segmentTrace = null;
        printTrace = null;
        pointCount = 0;

        postInvalidate();
    }

    public ArrayList<ArrayList<Point>> getAllTraces() {
        return allTraces;
    }

    public ArrayList<Point> handleMaxVelocityRadio(PointPair pointPair) {
        // Δy/Δx = r
        double r = Double.parseDouble(
                PreferenceManager.getDefaultSharedPreferences(getContext())
                        .getString(getResources().getString(R.string.MaxVelocityRadioKey),
                                getResources().getString(R.string.DefaultVelocityRadio)));


        Point start = pointPair.start;
        Point end = pointPair.end;

        ArrayList<Point> ans = new ArrayList<>();

        double delta_x = abs(start.x - end.x), delta_y = abs(start.y - end.y);

        ans.add(start);
//        if (abs(delta_x / delta_y) > r) {
//            ans.add(new Point(end.x - (int) delta_y, start.y));
//
//        } else if (abs(delta_y / delta_x) > r) {
//            ans.add(new Point(start.x, end.y - (int) delta_x));
//        }
        ans.add(end);
        return ans;
    }
}
