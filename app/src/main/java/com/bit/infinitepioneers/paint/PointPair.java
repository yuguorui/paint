package com.bit.infinitepioneers.paint;

/**
 * Created by yuguorui on 2016/7/24.
 * 点对，表示一条最简单的路径
 */
public class PointPair {
    public Point start;
    public Point end;

    public PointPair(Point start, Point end) {
        this.start = start;
        this.end = end;
    }
}
