package com.bit.infinitepioneers.paint;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;

/**
 * Created by yuguorui on 2016/7/22.
 * 可序列化的Bitmap
 */
public class BitmapSerializable implements Serializable {
    private static final long serialVersionUID = 1L;
    private byte[] bitmapBytes = null;

    public BitmapSerializable(Bitmap bitmap) {
        ByteArrayOutputStream baops = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, baops);
        bitmapBytes = baops.toByteArray();
    }

    public Bitmap getBitmap() {
        return BitmapFactory.decodeByteArray(bitmapBytes, 0, bitmapBytes.length);
    }
}
