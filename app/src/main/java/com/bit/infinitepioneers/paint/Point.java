package com.bit.infinitepioneers.paint;

import java.io.Serializable;

/**
 * Created by yuguorui on 2016/7/22.
 * 支持序列化的Point
 */
public class Point implements Serializable{
    private static final long serialVersionUID = 1L;

    public float x;
    public float y;

    public Point()
    {
        x = -1;
        y = -1;
    }

    public Point(android.graphics.Point point) {
        this.x = point.x;
        this.y = point.y;
    }

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Point(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public boolean equals(Point point) {
        return point.x == x && point.y == y;
    }
}
