package com.bit.infinitepioneers.paint;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;


/**
 * Created by yuguorui on 2016/7/23.
 * Card UI
 */
public class CardContentFragment extends Fragment {
    private final int FILE_UPDATE_REQUEST = 1;

    RecyclerView recyclerView;

    private int length = 0;
    private ArrayList<String> names;
    private ArrayList<Drawable> pictures;
    private ArrayList<ArrayList<ArrayList<Point>>> projects;
    private File[] files;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        names = new ArrayList<>();
        pictures = new ArrayList<>();
        projects = new ArrayList<>();
        recyclerView = (ContextMenuRecyclerView) inflater.inflate(R.layout.recycler_view,
                container, false);

        ContentAdapter adapter = new ContentAdapter(getActivity());
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setVerticalScrollBarEnabled(false);
        setHasOptionsMenu(true);

        registerForContextMenu(recyclerView);

        GridLayoutManager layoutManager = new GridLayoutManager(
                getActivity(),
                2,
                LinearLayoutManager.VERTICAL,
                false);


        recyclerView.setLayoutManager(layoutManager);
        return recyclerView;
    }


    public class ViewHolder extends RecyclerView.ViewHolder
            implements
            View.OnClickListener{
        public ImageView picture;
        public TextView name;


        public ViewHolder(final LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.item_card, parent, false));
            picture = (ImageView) itemView.findViewById(R.id.card_image);
            name = (TextView) itemView.findViewById(R.id.card_name);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Context context = view.getContext();
            Intent intent = new Intent(context, PaintActivity.class);
            intent.putExtra(PaintActivity.EXTRA_INDEX, getAdapterPosition());
            intent.putExtra(PaintActivity.EXTRA_DATA, projects.get(getAdapterPosition()));
            startActivityForResult(intent, FILE_UPDATE_REQUEST);
        }


    }

    @Override
    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        MenuInflater menuInflater = getActivity().getMenuInflater();
        menuInflater.inflate(R.menu.menu_file_list, contextMenu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        ContextMenuRecyclerView.RecyclerContextMenuInfo info = (ContextMenuRecyclerView.RecyclerContextMenuInfo) item.getMenuInfo();
        int index = info.position;
        switch (item.getItemId())
        {
            case R.id.delete_file:
                boolean delete = files[index - 1].delete();
                if(!delete) {
                    showTips(getString(R.string.TipWhenErrorDelete));
                }
                break;
            case R.id.edit_file:
                Context context = getActivity();
                Intent intent = new Intent(context, PaintActivity.class);
                intent.putExtra(PaintActivity.EXTRA_INDEX, index);
                intent.putExtra(PaintActivity.EXTRA_DATA, projects.get(index));
                startActivityForResult(intent, FILE_UPDATE_REQUEST);
                break;
            default:
                break;
        }
        onActivityResult(FILE_UPDATE_REQUEST, 0, null);
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case FILE_UPDATE_REQUEST:
                names = new ArrayList<>();
                pictures = new ArrayList<>();
                projects = new ArrayList<>();
                length = 0;
                recyclerView.setAdapter(new ContentAdapter(getActivity()));
                break;
            default:
                break;
        }
    }

    public class ContentAdapter extends RecyclerView.Adapter<ViewHolder> {
        public ContentAdapter(Context context) {
            // add the very first item
            names.add(context.getResources().getString(R.string.NewProjectTitle));
            pictures.add(context.getResources().getDrawable(R.drawable.new_icon, null));
            projects.add(null);
            length++;

            File root_dir = new File(context.getFilesDir(),
                    context.getResources().getString(R.string.ProjectsPath));
            files = root_dir.listFiles();
            if (files != null) {
                for (File file :
                        files) {
                    try {
                        ObjectInputStream in = new ObjectInputStream(new FileInputStream(file));
                        SavedObject savedObject = (SavedObject) in.readObject();
                        Drawable drawable = new BitmapDrawable(context.getResources(),
                                savedObject.bitmap.getBitmap());
                        String name = file.getName();
                        names.add(name);
                        pictures.add(drawable);
                        projects.add(savedObject.traces);
                        length++;
                    } catch (IOException | ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            }
        }




        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()), parent);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            holder.name.setText(names.get(position));
            if (position == 0) {
                holder.name.setBackgroundColor(
//                        CardContentFragment.this.getResources().getColor(R.color.colorAccent, null)
                        ContextCompat.getColor(getActivity(), R.color.colorAccent)
                );
                holder.picture.setImageDrawable(pictures.get(position));
            }
            else {
                holder.picture.setImageDrawable(pictures.get(position));
                holder.itemView.setLongClickable(true);
            }
        }


        @Override
        public int getItemCount() {
            return length;
        }
    }

    public void showTips(String content) {
        Snackbar.make(recyclerView, content, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }

}
