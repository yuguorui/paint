package com.bit.infinitepioneers.splash;

/**
 * Created by yuguorui on 2016/7/19.
 * Slash
 */

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;

import com.bit.infinitepioneers.paint.CardContainerActivity;
import com.bit.infinitepioneers.paint.CardContentFragment;
import com.bit.infinitepioneers.paint.PaintActivity;
import com.bit.infinitepioneers.paint.R;

public class Splash extends Activity {

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.splash);

        /* New Handler to start the Menu-Activity
         * and close this Splash-Screen after some seconds.*/
        /* Duration of wait */
        int SPLASH_DISPLAY_LENGTH = 1000;
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                /* Create an Intent that will start the Menu-Activity. */
//                Intent mainIntent = new Intent(Splash.this, PaintActivity.class);
                Intent mainIntent = new Intent(Splash.this, CardContainerActivity.class);
                Splash.this.startActivity(mainIntent);
                Splash.this.finish();
//                overridePendingTransition(R.anim.fadein,R.anim.fadeout);
            }
        }, SPLASH_DISPLAY_LENGTH);
    }
}
