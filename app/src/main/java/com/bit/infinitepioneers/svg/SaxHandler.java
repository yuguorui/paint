package com.bit.infinitepioneers.svg;

import android.util.Log;

import com.bit.infinitepioneers.paint.Point;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.Stack;

/**
 * Created by yuguorui on 2016/8/18.
 * SaxHandler
 */
public class SaxHandler extends DefaultHandler {
    public ArrayList<ArrayList<Point>> traces;
    private Stack<ArrayList<ArrayList<Point>>> tracesStack;

    public SaxHandler() {
        tracesStack = new Stack<>();
        traces = new ArrayList<>();
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        ArrayList<ArrayList<Point>> currentGroup = tracesStack.empty() ? traces : tracesStack.peek();
        ArrayList<Point> trace = new ArrayList<>();
        switch (qName) {
            case "g": {
                tracesStack.push(new ArrayList<ArrayList<Point>>());
                break;
            }

            // 线条
            case "line": {
                float x1 = Float.parseFloat(attributes.getValue("x1"));
                float x2 = Float.parseFloat(attributes.getValue("x2"));
                float y1 = Float.parseFloat(attributes.getValue("y1"));
                float y2 = Float.parseFloat(attributes.getValue("y2"));


                trace.add(new Point(x1, y1));
                trace.add(new Point(x2, y2));
                currentGroup.add(trace);
                break;
            }

            // 折线
            case "polyline": {
                String points = attributes.getValue("points");
                Scanner scanner = new Scanner(points);
                scanner.useDelimiter(",| ");
                while (scanner.hasNext()) {
                    if(scanner.hasNextFloat()) {
                        float x = scanner.nextFloat();
                        float y = scanner.nextFloat();
                        trace.add(new Point(x, y));
                    }
                    else
                        scanner.next();
                }
                currentGroup.add(trace);
                scanner.close();
                break;
            }

            // 多边形
            case "polygon": {
                String points = attributes.getValue("points");
                Scanner scanner = new Scanner(points);
                scanner.useDelimiter(",| ");
                Log.d("polygon", "start");
                while (scanner.hasNext()) {
                    if(scanner.hasNextFloat()) {
                        float x = scanner.nextFloat();
                        float y = scanner.nextFloat();
                        trace.add(new Point(x, y));
                    }
                    else
                        scanner.next();
                }
                trace.add(trace.get(0));
                currentGroup.add(trace);
                scanner.close();
                Log.d("polygon", "end");
                break;
            }

            // 矩形
            case "rect": {
                float x = Float.parseFloat(attributes.getValue("x"));
                float y = Float.parseFloat(attributes.getValue("y"));
                float width = Float.parseFloat(attributes.getValue("width"));
                float height = Float.parseFloat(attributes.getValue("height"));

                trace.add(new Point(x, y));
                trace.add(new Point(x + width, y));
                trace.add(new Point(x + width, y + height));
                trace.add(new Point(x, y));
                break;
            }

            // 圆形
            case "circle": {
                float cx = Float.parseFloat(attributes.getValue("cx"));
                float cy = Float.parseFloat(attributes.getValue("cy"));
                float r = Float.parseFloat(attributes.getValue("r"));

                trace.addAll(generateCircle(cx, cy, r));
                break;
            }

            // 椭圆
            case "ellipse": {
                float rx = Float.parseFloat(attributes.getValue("rx"));
                float ry = Float.parseFloat(attributes.getValue("ry"));
                float cx = Float.parseFloat(attributes.getValue("cx"));
                float cy = Float.parseFloat(attributes.getValue("cy"));

                trace.addAll(generateEllipse(rx, ry, cx, cy));
                break;
            }

            // 路径
            case "path": {
                String content = attributes.getValue("d");
                currentGroup.addAll(generatePath(content));
                break;
            }

            default:
                break;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        switch (qName) {
            case "g":
                ArrayList<ArrayList<Point>> data = tracesStack.empty() ? traces : tracesStack.pop();
                ArrayList<ArrayList<Point>> currentGroup = tracesStack.empty() ? traces : tracesStack.peek();
                // TODO 仿射变换的处理

                currentGroup.addAll(data);
                break;
            default:
                break;
        }
    }

    private ArrayList<Point> generateCircle(float cx, float cy, float r) {
        ArrayList<Point> circles = new ArrayList<>();
        // 圆的细分数
        final int subdivision = 32;
        double delta = 2.0 / subdivision * Math.PI;
        for (int i = 0; i < subdivision; i++) {
            circles.add(new Point(
                    (float) (r * Math.cos(delta * i) + cx),
                    (float) (r * Math.sin(delta * i) + cy)
            ));
        }
        circles.add(circles.get(0));
        return circles;
    }

    private ArrayList<Point> generateEllipse(float rx, float ry, float cx, float cy) {
        ArrayList<Point> ellipse = new ArrayList<>();
        // 圆的细分数
        final int subdivision = 32;
        double delta = 2.0 / subdivision * Math.PI;
        for (int i = 0; i < subdivision; i++) {
            ellipse.add(new Point(
                    (float) (cx + rx * Math.cos(delta * i)),
                    (float) (cy + ry * Math.sin((delta * i)))
            ));
        }
        ellipse.add(ellipse.get(0));
        return ellipse;
    }

    private ArrayList<ArrayList<Point>> generatePath(String content) {
//      final int subdivision = 32;
        return PathParser.parser(content);
    }
}
