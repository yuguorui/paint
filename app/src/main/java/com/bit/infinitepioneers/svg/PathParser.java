/* PathParser.java */
/* Generated By:JavaCC: Do not edit this line. PathParser.java */
package com.bit.infinitepioneers.svg;

import java.io.*;
import java.util.ArrayList;
import com.bit.infinitepioneers.paint.Point;

import android.util.Log;

public class PathParser implements PathParserConstants {
    public static int CubicSubdivision = 8;

    public Point currentPoint;
    public Point initialPoint;
    public Point lastSecondControlPoint;

    public PathParser() {
        currentPoint = new Point(-1, -1);
        initialPoint = new Point(-1, -1);
        lastSecondControlPoint = new Point(-1, -1);
    }

    static public ArrayList<ArrayList<Point>> parser(String content) {
        Reader reader = null;
        ArrayList<ArrayList<Point>> traces = null;
        try {
            reader = new StringReader(content);
            traces = new PathParser(reader).pathdata();
        }
        catch (ParseException ex) {
            ex.printStackTrace();
        }
        return traces;
    }

    public Point cubicBezierSinglePoint(Point start, Point c1, Point c2, Point end, double t)
    {
        Point answer = new Point();
        double x = (double)start.x * Math.pow((1 - t), 3)
                + (double)c1.x * 3 * t * Math.pow((1 - t), 2)
                + (double)c2.x * 3 * t * t * (1 - t)
                + (double)end.x * t * t * t;
        double y = (double)start.y * Math.pow((1 - t), 3)
                + (double)c1.y * 3 * t * Math.pow((1 - t), 2)
                + (double)c2.y * 3 * t * t * (1 - t)
                + (double)end.y * t * t * t;

        answer.x = (float) x;
        answer.y = (float) y;
        return answer;
    }

    public ArrayList<Point> cubicBezier(Point start, Point c1, Point c2, Point end)
    {
        ArrayList<Point> answer = new ArrayList<Point>();

        double delta = 1.0 / CubicSubdivision;
        for (int j = 0; j <= CubicSubdivision; j++)
        {
            answer.add(cubicBezierSinglePoint(start, c1, c2, end, delta * j));
        }

        return answer;
    }

  final public ArrayList<ArrayList<Point>> pathdata() throws ParseException {ArrayList<ArrayList<Point>> summary = new ArrayList<ArrayList<Point>>();
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case MOVE:{
      summary = moveto_drawto_command_groups();
      break;
      }
    default:
      jj_la1[0] = jj_gen;
      ;
    }
    jj_consume_token(0);
{if ("" != null) return summary;}
    throw new Error("Missing return statement in function");
  }

  final public ArrayList<ArrayList<Point>> moveto_drawto_command_groups() throws ParseException {ArrayList<ArrayList<Point>> summary = new ArrayList<ArrayList<Point>>();
    ArrayList<ArrayList<Point>> group = new ArrayList<ArrayList<Point>>();
    label_1:
    while (true) {
      group = moveto_drawto_command_group();
summary.addAll(group);
      switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
      case MOVE:{
        ;
        break;
        }
      default:
        jj_la1[1] = jj_gen;
        break label_1;
      }
    }
{if ("" != null) return summary;}
    throw new Error("Missing return statement in function");
  }

  final public ArrayList<ArrayList<Point>> moveto_drawto_command_group() throws ParseException {ArrayList<ArrayList<Point>> summary = new ArrayList<ArrayList<Point>>();
    ArrayList<Point> movePoints = new ArrayList<Point>();
    ArrayList<ArrayList<Point>> drawPoints = new ArrayList<ArrayList<Point>>();
    movePoints = moveto();
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case LINE:
    case HORIZONTAL:
    case VERTICAL:
    case CURVE:
    case SMOOTH_CURVE:
    case 10:
    case 11:
    case 12:
    case 13:
    case 14:
    case 15:{
      drawPoints = drawto_commands();
      break;
      }
    default:
      jj_la1[2] = jj_gen;
      ;
    }
summary.add(movePoints);
        if(drawPoints.size() != 0)
        {
            summary.addAll(drawPoints);
        }
        {if ("" != null) return summary;}
    throw new Error("Missing return statement in function");
  }

  final public ArrayList<ArrayList<Point>> drawto_commands() throws ParseException {ArrayList<ArrayList<Point>> summary = new ArrayList<ArrayList<Point>>();
    ArrayList<ArrayList<Point>> tmp = new ArrayList<ArrayList<Point>>();
    label_2:
    while (true) {
      tmp = drawto_command();
summary.addAll(tmp);
      switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
      case LINE:
      case HORIZONTAL:
      case VERTICAL:
      case CURVE:
      case SMOOTH_CURVE:
      case 10:
      case 11:
      case 12:
      case 13:
      case 14:
      case 15:{
        ;
        break;
        }
      default:
        jj_la1[3] = jj_gen;
        break label_2;
      }
    }
{if ("" != null) return summary;}
    throw new Error("Missing return statement in function");
  }

  final public ArrayList<ArrayList<Point>> drawto_command() throws ParseException {ArrayList<ArrayList<Point>> summary = new ArrayList<ArrayList<Point>>();
    ArrayList<Point> closePoints = new ArrayList<Point>();
    ArrayList<Point> linePoints = new ArrayList<Point>();
    ArrayList<Point> horizontalPoints = new ArrayList<Point>();
    ArrayList<Point> verticalPoints = new ArrayList<Point>();
    ArrayList<Point> curvePoints = new ArrayList<Point>();
    ArrayList<Point> smoothPoints = new ArrayList<Point>();
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case 10:
    case 11:{
      closePoints = closepath();
      break;
      }
    case LINE:{
      linePoints = lineto();
      break;
      }
    case HORIZONTAL:{
      horizontalPoints = horizontal_lineto();
      break;
      }
    case VERTICAL:{
      verticalPoints = vertical_lineto();
      break;
      }
    case CURVE:{
      curvePoints = curveto();
      break;
      }
    case SMOOTH_CURVE:{
      smoothPoints = smooth_curveto();
      break;
      }
    case 12:
    case 13:{
      quadratic_bezier_curveto();
      break;
      }
    case 14:
    case 15:{
      smooth_quadratic_bezier_curveto();
      break;
      }
    default:
      jj_la1[4] = jj_gen;
      jj_consume_token(-1);
      throw new ParseException();
    }
if(closePoints.size()!=0)
        {
            summary.add(closePoints);
        }
        if(linePoints.size()!=0)
        {
            summary.add(linePoints);
        }
        if(horizontalPoints.size()!=0)
        {
            summary.add(horizontalPoints);
        }
        if(verticalPoints.size()!=0)
        {
            summary.add(verticalPoints);
        }
        if(curvePoints.size()!=0)
        {
            summary.add(curvePoints);
        }
        if(smoothPoints.size()!=0)
        {
            summary.add(smoothPoints);
        }
        {if ("" != null) return summary;}
    throw new Error("Missing return statement in function");
  }

  final public ArrayList<Point> moveto() throws ParseException {Token t;
    ArrayList<Point> tmpPointArray = new ArrayList<Point>();
    t = jj_consume_token(MOVE);
    tmpPointArray = moveto_argument_sequence();
if(t.image.equals("m"))
        {
            for (int i = 0; i < tmpPointArray.size(); i++)
            {
                tmpPointArray.get(i).x += currentPoint.x;
                tmpPointArray.get(i).y += currentPoint.y;
            }
        }

        currentPoint = tmpPointArray.get(tmpPointArray.size() - 1);
        //  第一个点作为closePath的初始点
        initialPoint = tmpPointArray.get(0);
        {if ("" != null) return tmpPointArray;}
    throw new Error("Missing return statement in function");
  }

  final public ArrayList<Point> moveto_argument_sequence() throws ParseException {Point tmpPoint;
    ArrayList<Point> tmpPointArray = new ArrayList<Point>();
    tmpPoint = coordinate_pair();
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case 9:{
      jj_consume_token(9);
      break;
      }
    default:
      jj_la1[5] = jj_gen;
      ;
    }
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case number:{
      tmpPointArray = lineto_argument_sequence();
      break;
      }
    default:
      jj_la1[6] = jj_gen;
      ;
    }
tmpPointArray.add(0, tmpPoint);
        {if ("" != null) return tmpPointArray;}
    throw new Error("Missing return statement in function");
  }

  final public ArrayList<Point> closepath() throws ParseException {
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case 10:{
      jj_consume_token(10);
      break;
      }
    case 11:{
      jj_consume_token(11);
      break;
      }
    default:
      jj_la1[7] = jj_gen;
      jj_consume_token(-1);
      throw new ParseException();
    }
ArrayList<Point> tmpPointArray = new ArrayList<Point>();
        tmpPointArray.add(currentPoint);
        tmpPointArray.add(initialPoint);
        currentPoint = initialPoint;
        {if ("" != null) return tmpPointArray;}
    throw new Error("Missing return statement in function");
  }

  final public ArrayList<Point> lineto() throws ParseException {ArrayList<Point> pointArray;
    Token t;
    t = jj_consume_token(LINE);
    pointArray = lineto_argument_sequence();
if(t.image.equals("l"))
        {
            for(int i = 0; i < pointArray.size(); i++)
            {
                pointArray.get(i).x += currentPoint.x;
                pointArray.get(i).y += currentPoint.y;
            }
        }
        pointArray.add(0, currentPoint);
        currentPoint = pointArray.get(pointArray.size() - 1);
        {if ("" != null) return pointArray;}
    throw new Error("Missing return statement in function");
  }

  final public ArrayList<Point> lineto_argument_sequence() throws ParseException {Point tmpPoint;
    ArrayList<Point> pointArray = new ArrayList<Point>();
    ArrayList<Point> tmpPointArray = new ArrayList<Point>();
    tmpPoint = coordinate_pair();
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case 9:{
      jj_consume_token(9);
      break;
      }
    default:
      jj_la1[8] = jj_gen;
      ;
    }
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case number:{
      tmpPointArray = lineto_argument_sequence();
      break;
      }
    default:
      jj_la1[9] = jj_gen;
      ;
    }
pointArray.add(tmpPoint);
        pointArray.addAll(tmpPointArray);
        {if ("" != null) return pointArray;}
    throw new Error("Missing return statement in function");
  }

  final public ArrayList<Point> horizontal_lineto() throws ParseException {Token t;
    ArrayList<Float> tmpFloatArray = new ArrayList<Float>();
    t = jj_consume_token(HORIZONTAL);
    tmpFloatArray = horizontal_lineto_argument_sequence();
if(t.image.equals("h"))
        {
            tmpFloatArray.set(0, tmpFloatArray.get(0) + currentPoint.x);
            for (int i = 1; i < tmpFloatArray.size(); i++)
            {
                tmpFloatArray.set(i, tmpFloatArray.get(i) + tmpFloatArray.get(i - 1));
            }
        }

        ArrayList<Point> tmpPointArray = new ArrayList<Point>();
        tmpPointArray.add(currentPoint);
        for (int i = 0; i < tmpFloatArray.size(); i++)
        {
            tmpPointArray.add(new Point(tmpFloatArray.get(i).floatValue(), currentPoint.y));
        }

        currentPoint = tmpPointArray.get(tmpPointArray.size() - 1);
        {if ("" != null) return tmpPointArray;}
    throw new Error("Missing return statement in function");
  }

  final public ArrayList<Float> horizontal_lineto_argument_sequence() throws ParseException {float tmpFloat;
    ArrayList<Float> tmpFloatArray = new ArrayList<Float>();
    tmpFloat = coordinate();
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case 9:{
      jj_consume_token(9);
      break;
      }
    default:
      jj_la1[10] = jj_gen;
      ;
    }
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case number:{
      tmpFloatArray = horizontal_lineto_argument_sequence();
      break;
      }
    default:
      jj_la1[11] = jj_gen;
      ;
    }
tmpFloatArray.add(0, tmpFloat);
        {if ("" != null) return tmpFloatArray;}
    throw new Error("Missing return statement in function");
  }

  final public ArrayList<Point> vertical_lineto() throws ParseException {Token t;
    ArrayList<Float> tmpFloatArray = new ArrayList<Float>();
    t = jj_consume_token(VERTICAL);
    tmpFloatArray = vertical_lineto_argument_sequence();
if(t.image.equals("v"))
        {
            tmpFloatArray.set(0, tmpFloatArray.get(0) + currentPoint.y);
            for (int i = 1; i < tmpFloatArray.size(); i++)
            {
                tmpFloatArray.set(i, tmpFloatArray.get(i) + tmpFloatArray.get(i - 1));
            }
        }

        ArrayList<Point> tmpPointArray = new ArrayList<Point>();
        tmpPointArray.add(currentPoint);
        for (int i = 0; i < tmpFloatArray.size(); i++)
        {
            tmpPointArray.add(new Point(currentPoint.x, tmpFloatArray.get(i).floatValue()));
        }

        currentPoint = tmpPointArray.get(tmpPointArray.size() - 1);
        {if ("" != null) return tmpPointArray;}
    throw new Error("Missing return statement in function");
  }

  final public ArrayList<Float> vertical_lineto_argument_sequence() throws ParseException {float tmpFloat;
    ArrayList<Float> tmpFloatArray = new ArrayList<Float>();
    tmpFloat = coordinate();
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case 9:{
      jj_consume_token(9);
      break;
      }
    default:
      jj_la1[12] = jj_gen;
      ;
    }
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case number:{
      tmpFloatArray = vertical_lineto_argument_sequence();
      break;
      }
    default:
      jj_la1[13] = jj_gen;
      ;
    }
tmpFloatArray.add(0, tmpFloat);
        {if ("" != null) return tmpFloatArray;}
    throw new Error("Missing return statement in function");
  }

  final public ArrayList<Point> curveto() throws ParseException {Token t;
    ArrayList<Point> tmpPointArray;
    t = jj_consume_token(CURVE);
    tmpPointArray = curveto_argument_sequence();
if(t.image.equals("c"))
        {
            for (int i = 0; i < tmpPointArray.size(); i++)
            {
                tmpPointArray.get(i).x += currentPoint.x;
                tmpPointArray.get(i).y += currentPoint.y;
            }
        }

        ArrayList<Point> answer = new ArrayList<Point>();

        for(int i = 0; i < tmpPointArray.size(); i+=3)
        {
            answer.addAll(cubicBezier(currentPoint,
                                    tmpPointArray.get(i),
                                    tmpPointArray.get(i + 1),
                                    tmpPointArray.get(i + 2)));
            lastSecondControlPoint = tmpPointArray.get(i + 1);
        }
        currentPoint = tmpPointArray.get(tmpPointArray.size() - 1);
        {if ("" != null) return answer;}
    throw new Error("Missing return statement in function");
  }

  final public ArrayList<Point> curveto_argument_sequence() throws ParseException {ArrayList<Point> first = new ArrayList<Point>();
    ArrayList<Point> fllow_up = new ArrayList<Point>();
    first = curveto_argument();
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case 9:{
      jj_consume_token(9);
      break;
      }
    default:
      jj_la1[14] = jj_gen;
      ;
    }
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case number:{
      fllow_up = curveto_argument_sequence();
      break;
      }
    default:
      jj_la1[15] = jj_gen;
      ;
    }
// 三次贝塞尔曲线的参数是三个控制点构成的三元组
        ArrayList<Point> answer = new ArrayList<Point>();
        answer.addAll(first);
        answer.addAll(fllow_up);
        {if ("" != null) return answer;}
    throw new Error("Missing return statement in function");
  }

  final public ArrayList<Point> curveto_argument() throws ParseException {Point a,b,c;
    a = coordinate_pair();
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case 9:{
      jj_consume_token(9);
      break;
      }
    default:
      jj_la1[16] = jj_gen;
      ;
    }
    b = coordinate_pair();
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case 9:{
      jj_consume_token(9);
      break;
      }
    default:
      jj_la1[17] = jj_gen;
      ;
    }
    c = coordinate_pair();
ArrayList<Point> answer = new ArrayList<Point>();
        answer.add(a);
        answer.add(b);
        answer.add(c);
        {if ("" != null) return answer;}
    throw new Error("Missing return statement in function");
  }

  final public ArrayList<Point> smooth_curveto() throws ParseException {Token t;
    ArrayList<Point> tmpPointArray = new ArrayList<Point>();
    ArrayList<Point> answer = new ArrayList<Point>();
    t = jj_consume_token(SMOOTH_CURVE);
    tmpPointArray = smooth_curveto_argument_sequence();
if(t.image.equals("s"))
        {
            for (int i = 0; i < tmpPointArray.size(); i++)
            {
                tmpPointArray.get(i).x += currentPoint.x;
                tmpPointArray.get(i).y += currentPoint.y;
            }
        }

        for(int i = 0; i < tmpPointArray.size(); i+=2)
        {
            Point c1 = new Point();
            if(lastSecondControlPoint.x == -1)
            {
                c1 = currentPoint;
            }
            else
            {
                c1.x = currentPoint.x * 2 - lastSecondControlPoint.x;
                c1.y = currentPoint.y * 2 - lastSecondControlPoint.y;
            }
            answer.addAll(cubicBezier(currentPoint,
                                    c1,
                                    tmpPointArray.get(i),
                                    tmpPointArray.get(i + 1)));
            lastSecondControlPoint = tmpPointArray.get(i);
        }
        currentPoint = tmpPointArray.get(tmpPointArray.size() - 1);
        {if ("" != null) return answer;}
    throw new Error("Missing return statement in function");
  }

  final public ArrayList<Point> smooth_curveto_argument_sequence() throws ParseException {ArrayList<Point> first = new ArrayList<Point>();
    ArrayList<Point> fllow_up = new ArrayList<Point>();
    first = smooth_curveto_argument();
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case 9:{
      jj_consume_token(9);
      break;
      }
    default:
      jj_la1[18] = jj_gen;
      ;
    }
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case number:{
      fllow_up = smooth_curveto_argument_sequence();
      break;
      }
    default:
      jj_la1[19] = jj_gen;
      ;
    }
ArrayList<Point> answer = new ArrayList<Point>();
        answer.addAll(first);
        answer.addAll(fllow_up);
        {if ("" != null) return answer;}
    throw new Error("Missing return statement in function");
  }

  final public ArrayList<Point> smooth_curveto_argument() throws ParseException {Point first, later;
    first = coordinate_pair();
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case 9:{
      jj_consume_token(9);
      break;
      }
    default:
      jj_la1[20] = jj_gen;
      ;
    }
    later = coordinate_pair();
ArrayList<Point> answer = new ArrayList<Point>();
        answer.add(first);
        answer.add(later);
        {if ("" != null) return answer;}
    throw new Error("Missing return statement in function");
  }

  final public void quadratic_bezier_curveto() throws ParseException {
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case 12:{
      jj_consume_token(12);
      break;
      }
    case 13:{
      jj_consume_token(13);
      break;
      }
    default:
      jj_la1[21] = jj_gen;
      jj_consume_token(-1);
      throw new ParseException();
    }
    quadratic_bezier_curveto_argument_sequence();

  }

  final public void quadratic_bezier_curveto_argument_sequence() throws ParseException {
    quadratic_bezier_curveto_argument();
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case 9:{
      jj_consume_token(9);
      break;
      }
    default:
      jj_la1[22] = jj_gen;
      ;
    }
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case number:{
      quadratic_bezier_curveto_argument_sequence();
      break;
      }
    default:
      jj_la1[23] = jj_gen;
      ;
    }

  }

  final public void quadratic_bezier_curveto_argument() throws ParseException {
    coordinate_pair();
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case 9:{
      jj_consume_token(9);
      break;
      }
    default:
      jj_la1[24] = jj_gen;
      ;
    }
    coordinate_pair();

  }

  final public void smooth_quadratic_bezier_curveto() throws ParseException {
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case 14:{
      jj_consume_token(14);
      break;
      }
    case 15:{
      jj_consume_token(15);
      break;
      }
    default:
      jj_la1[25] = jj_gen;
      jj_consume_token(-1);
      throw new ParseException();
    }
    smooth_quadratic_bezier_curveto_argument_sequence();

  }

  final public void smooth_quadratic_bezier_curveto_argument_sequence() throws ParseException {
    coordinate_pair();
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case number:{
      smooth_quadratic_bezier_curveto_argument_sequence();
      break;
      }
    default:
      jj_la1[26] = jj_gen;
      ;
    }

  }

  final public Point coordinate_pair() throws ParseException {float x, y;
    x = coordinate();
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case 9:{
      jj_consume_token(9);
      break;
      }
    default:
      jj_la1[27] = jj_gen;
      ;
    }
    y = coordinate();
{if ("" != null) return new Point(x, y);}
    throw new Error("Missing return statement in function");
  }

  final public float coordinate() throws ParseException {Token t;
    t = jj_consume_token(number);
Log.d("coordinate", t.image);
        {if ("" != null) return Float.parseFloat(t.image);}
    throw new Error("Missing return statement in function");
  }

  /** Generated Token Manager. */
  public PathParserTokenManager token_source;
  SimpleCharStream jj_input_stream;
  /** Current token. */
  public Token token;
  /** Next token. */
  public Token jj_nt;
  private int jj_ntk;
  private int jj_gen;
  final private int[] jj_la1 = new int[28];
  static private int[] jj_la1_0;
  static {
      jj_la1_init_0();
   }
   private static void jj_la1_init_0() {
      jj_la1_0 = new int[] {0x8,0x8,0xfdf0,0xfdf0,0xfdf0,0x200,0x4,0xc00,0x200,0x4,0x200,0x4,0x200,0x4,0x200,0x4,0x200,0x200,0x200,0x4,0x200,0x3000,0x200,0x4,0x200,0xc000,0x4,0x200,};
   }

  /** Constructor with InputStream. */
  public PathParser(java.io.InputStream stream) {
     this(stream, null);
  }
  /** Constructor with InputStream and supplied encoding */
  public PathParser(java.io.InputStream stream, String encoding) {
    try { jj_input_stream = new SimpleCharStream(stream, encoding, 1, 1); } catch(java.io.UnsupportedEncodingException e) { throw new RuntimeException(e); }
    token_source = new PathParserTokenManager(jj_input_stream);
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 28; i++) jj_la1[i] = -1;
  }

  /** Reinitialise. */
  public void ReInit(java.io.InputStream stream) {
     ReInit(stream, null);
  }
  /** Reinitialise. */
  public void ReInit(java.io.InputStream stream, String encoding) {
    try { jj_input_stream.ReInit(stream, encoding, 1, 1); } catch(java.io.UnsupportedEncodingException e) { throw new RuntimeException(e); }
    token_source.ReInit(jj_input_stream);
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 28; i++) jj_la1[i] = -1;
  }

  /** Constructor. */
  public PathParser(java.io.Reader stream) {
    jj_input_stream = new SimpleCharStream(stream, 1, 1);
    token_source = new PathParserTokenManager(jj_input_stream);
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 28; i++) jj_la1[i] = -1;
  }

  /** Reinitialise. */
  public void ReInit(java.io.Reader stream) {
    jj_input_stream.ReInit(stream, 1, 1);
    token_source.ReInit(jj_input_stream);
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 28; i++) jj_la1[i] = -1;
  }

  /** Constructor with generated Token Manager. */
  public PathParser(PathParserTokenManager tm) {
    token_source = tm;
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 28; i++) jj_la1[i] = -1;
  }

  /** Reinitialise. */
  public void ReInit(PathParserTokenManager tm) {
    token_source = tm;
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 28; i++) jj_la1[i] = -1;
  }

  private Token jj_consume_token(int kind) throws ParseException {
    Token oldToken;
    if ((oldToken = token).next != null) token = token.next;
    else token = token.next = token_source.getNextToken();
    jj_ntk = -1;
    if (token.kind == kind) {
      jj_gen++;
      return token;
    }
    token = oldToken;
    jj_kind = kind;
    throw generateParseException();
  }


/** Get the next Token. */
  final public Token getNextToken() {
    if (token.next != null) token = token.next;
    else token = token.next = token_source.getNextToken();
    jj_ntk = -1;
    jj_gen++;
    return token;
  }

/** Get the specific Token. */
  final public Token getToken(int index) {
    Token t = token;
    for (int i = 0; i < index; i++) {
      if (t.next != null) t = t.next;
      else t = t.next = token_source.getNextToken();
    }
    return t;
  }

  private int jj_ntk_f() {
    if ((jj_nt=token.next) == null)
      return (jj_ntk = (token.next=token_source.getNextToken()).kind);
    else
      return (jj_ntk = jj_nt.kind);
  }

  private java.util.List<int[]> jj_expentries = new java.util.ArrayList<int[]>();
  private int[] jj_expentry;
  private int jj_kind = -1;

  /** Generate ParseException. */
  public ParseException generateParseException() {
    jj_expentries.clear();
    boolean[] la1tokens = new boolean[16];
    if (jj_kind >= 0) {
      la1tokens[jj_kind] = true;
      jj_kind = -1;
    }
    for (int i = 0; i < 28; i++) {
      if (jj_la1[i] == jj_gen) {
        for (int j = 0; j < 32; j++) {
          if ((jj_la1_0[i] & (1<<j)) != 0) {
            la1tokens[j] = true;
          }
        }
      }
    }
    for (int i = 0; i < 16; i++) {
      if (la1tokens[i]) {
        jj_expentry = new int[1];
        jj_expentry[0] = i;
        jj_expentries.add(jj_expentry);
      }
    }
    int[][] exptokseq = new int[jj_expentries.size()][];
    for (int i = 0; i < jj_expentries.size(); i++) {
      exptokseq[i] = jj_expentries.get(i);
    }
    return new ParseException(token, exptokseq, tokenImage);
  }

  /** Enable tracing. */
  final public void enable_tracing() {
  }

  /** Disable tracing. */
  final public void disable_tracing() {
  }

}
