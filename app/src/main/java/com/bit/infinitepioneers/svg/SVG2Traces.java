package com.bit.infinitepioneers.svg;

import android.preference.PreferenceManager;
import android.util.Log;

import com.bit.infinitepioneers.paint.Point;

import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;


/**
 * Created by yuguorui on 2016/8/16.
 * convert SVG to points
 */
public class SVG2Traces {
    static public ArrayList<ArrayList<Point>> SVG2Trace(String filename) {
        // 1.实例化SAXParserFactory对象
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser parser;
        SaxHandler dh = null;
        try {
            // 2.创建解析器
            parser = factory.newSAXParser();
            // 3.获取需要解析的文档，生成解析器,最后解析文档
            File f = new File(filename);
            dh = new SaxHandler();
            Log.d("SVG", "Start");
            parser.parse(f, dh);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
        return simplifyTrace(dh.traces);
    }

    static public ArrayList<ArrayList<Point>> simplifyTrace(ArrayList<ArrayList<Point>> traces) {
        ArrayList<ArrayList<Point>> answer = new ArrayList<>();
        for (ArrayList<Point> trace :
                traces) {
            if (trace.size() > 0) {
                ArrayList<Point> tmpTrace = new ArrayList<>();
                Point firstPoint = trace.get(0);
                tmpTrace.add(firstPoint);
                for (int i = 1; i < trace.size(); i++) {
                    if (!firstPoint.equals(trace.get(i))) {
                        firstPoint = trace.get(i);
                        tmpTrace.add(trace.get(i));
                    }
                }
                answer.add(tmpTrace);
            }
        }
        return answer;
    }

}


